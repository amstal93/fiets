package ce.info.fiets.infrastructure;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Test;

class JsoupConnectionTest {
  private JsoupConnection cut = new JsoupConnection();

  @Test
  void doesGetDocument() throws IOException {
    Document document = cut.get("http://de.wikipedia.org/wiki/Ontologie");
    assertThat(document).isNotNull();
    assertThat(document.title()).contains("Ontologie – Wikipedia");
  }

}
