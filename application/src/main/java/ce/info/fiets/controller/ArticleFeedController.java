package ce.info.fiets.controller;

import ce.info.fiets.adapter.ArticleSource;
import ce.info.fiets.domain.Article;

import java.time.LocalTime;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.View;

@RestController
@RequiredArgsConstructor
@RequestMapping("api")
@Validated
public class ArticleFeedController {

  @NonNull
  private final ArticleSource articleSource;

  @GetMapping(path = "wikipedia/{language}/{category}", produces = "application/atom+xml")
  public View getWikipedia(
      @PathVariable("language")
      @Size(min = 1, max = 5)
          String language, // size< 5
      @PathVariable("category")
      @Size(max = 100) @NotBlank
          String category, // size< 100
      @RequestParam(name = "updateTime", defaultValue = "05:00")
      @Pattern(regexp = "[0-2][0-9]:[0-5][0-9]")
          String updateTime,
      @RequestParam(name = "depth", defaultValue = "5")
      @Max(50)
          int depth
  ) {
    LocalTime requestedUpdateTime = LocalTime.parse(updateTime);
    Article article = articleSource.randomWikipediaArticleFor(language, category, requestedUpdateTime, depth);
    return new ArticleFeedView(article);
  }

  @GetMapping(path = "article", produces = "application/atom+xml")
  public View getArticle(@RequestParam(name = "updateTime", defaultValue = "05:00")
                         @Pattern(regexp = "[0-2][0-9]:[0-5][0-9]")
                             String updateTime,
                         @RequestParam(name = "allowScripting", defaultValue = "false")
                             boolean allowScripting,
                         @RequestParam(name = "contentUrl")
                         @Size(min = 5, max = 256)
                             String contentUrl
  ) {
    LocalTime requestedUpdateTime = LocalTime.parse(updateTime);
    Article article = articleSource.sameArticleFrom(contentUrl, requestedUpdateTime, allowScripting);
    return new ArticleFeedView(article);
  }

  @GetMapping(path = "linkFrom", produces = "application/atom+xml")
  public View getLinkFrom(
      @RequestParam(name = "updateTime", defaultValue = "05:00")
      @Pattern(regexp = "[0-2][0-9]:[0-5][0-9]")
          String updateTime,
      @RequestParam(name = "url")
      @Size(min = 5, max = 256)
          String url,
      @RequestParam(name = "cssid", defaultValue = "")
      @Size(max = 256)
          String cssId
  ) {
    LocalTime requestedUpdateTime = LocalTime.parse(updateTime);
    String clearedCssId = cssId.replace("\"", "");
    Article article = articleSource.randomLinkFrom(url, clearedCssId, requestedUpdateTime);
    return new ArticleFeedView(article);
  }
}
