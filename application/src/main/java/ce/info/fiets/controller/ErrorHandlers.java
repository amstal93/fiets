package ce.info.fiets.controller;

import ce.info.fiets.domain.RemoteException;

import java.util.Objects;
import javax.validation.ConstraintViolationException;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
@Log4j2
public class ErrorHandlers {

  @ExceptionHandler(ConstraintViolationException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  ResponseEntity<String> handleConstraintViolationException(ConstraintViolationException cex) {
    return new ResponseEntity<>(cex.getMessage(), HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(IllegalArgumentException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  ResponseEntity<String> handleIllegalArgumentException(IllegalArgumentException cex) {
    log.info(cex.getMessage());
    return new ResponseEntity<>(cex.getMessage(), HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(RemoteException.class)
  @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
  ResponseEntity<String> handleRemoteException(RemoteException cex) {
    if (Objects.isNull(cex.getCause())) {
      log.warn(cex.getMessage());
    } else {
      log.error(cex.getMessage(), cex.getCause());
    }
    return new ResponseEntity<>(cex.getMessage(), HttpStatus.SERVICE_UNAVAILABLE);
  }

}
