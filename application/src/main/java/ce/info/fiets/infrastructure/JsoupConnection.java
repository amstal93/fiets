package ce.info.fiets.infrastructure;

import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

@Component
public class JsoupConnection {
  public Document get(String url) throws IOException {
    return Jsoup.connect(url).get();
  }
}
