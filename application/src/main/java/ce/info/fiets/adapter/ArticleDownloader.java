package ce.info.fiets.adapter;

import static java.util.function.Predicate.not;

import ce.info.fiets.domain.Article;
import ce.info.fiets.infrastructure.HttpSource;

import java.security.SecureRandom;
import java.time.Instant;
import java.util.List;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class ArticleDownloader {

  private final HttpSource httpSource;

  private final SecureRandom random;

  public ArticleDownloader(@NonNull HttpSource httpSource) {
    this.httpSource = httpSource;
    this.random = new SecureRandom();
  }

  public Article fetchArticle(String contentUrl, boolean allowScripting) {
    log.info("Read article from " + contentUrl);

    Document document = allowScripting ? httpSource.getScriptedFrom(contentUrl) : httpSource.getFrom(contentUrl);
    return Article.builder()
        .feedTitle(contentUrl + " - daily article")
        .lastUpdate(Instant.now())
        .title(document.title())
        .htmlContent(document.html())
        .contentLink(contentUrl)
        .build();
  }

  public Article randomLinkFrom(String linkArticleUrl, String cssId) {
    log.info("Fetch link from " + linkArticleUrl);
    Element startPage = httpSource.getFrom(linkArticleUrl);

    Element linkArea = selectArea(startPage, cssId);

    Elements links = linkArea.select("a[href]");
    List<String> targetUrls = links.stream()
        .map(l -> l.attr("abs:href"))
        .filter(not(String::isEmpty))
        .toList();

    log.info("Select one article from {} links", targetUrls::size);

    String targetUrl = targetUrls.get(random.nextInt(targetUrls.size()));
    return fetchArticle(targetUrl, false);
  }

  private Element selectArea(Element startPage, String cssId) {
    if (cssId.isBlank()) {
      return startPage;
    } else {
      Element area = startPage.getElementById(cssId);
      if (area == null) {
        area = startPage.selectFirst(cssId);
        if (area == null) {
          log.warn("No element found for {}", cssId);
          area = startPage;
        }
      }
      return area;
    }
  }
}
