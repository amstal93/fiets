package ce.info.fiets.domain;

import java.time.Instant;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder(toBuilder = true)
@Document(collection = "articles")
public class Article {
  @Id
  String id;

  @Accessors(chain = true)
  String key;
  String title;
  String htmlContent;
  String contentLink;
  String feedTitle;
  @EqualsAndHashCode.Exclude
  Instant lastUpdate;

  public boolean isExpired(ZonedDateTime now, LocalTime updateTime) {
    ZonedDateTime todayAtUpdateTime = now
        .with(ChronoField.HOUR_OF_DAY, updateTime.getHour())
        .with(ChronoField.MINUTE_OF_HOUR, updateTime.getMinute())
        .truncatedTo(ChronoUnit.MINUTES);
    ZonedDateTime yesterdayAtUpdateTime = todayAtUpdateTime
        .minus(1, ChronoUnit.DAYS)
        .truncatedTo(ChronoUnit.MINUTES);

    return lastUpdate.isBefore(yesterdayAtUpdateTime.toInstant())
        || now.isAfter(todayAtUpdateTime) && !lastUpdate.isAfter(todayAtUpdateTime.toInstant());
  }

}
