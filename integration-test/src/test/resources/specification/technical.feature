@Technical
Feature: Test technical features

  Scenario: It provides a landing page
    When I get "readme.html" from service
    Then I receive a response with status 200
    And the content type is text/html

  Scenario: It can be pinged
    When I get "/api/ping" from service
    Then I receive a response with status 200

  Scenario: It provides a favicon
    When I get "/favicon.ico" from service
    Then I receive a response with status 200

  Scenario: Expose prometheus endpoint
    When I get "/prometheus" from management
    Then I receive a response with status 200
    And the response contains the text "system_cpu_count"
    And the response contains the text "jvm_buffer_memory_used_bytes"
    And the response contains the text "logback_events_total"

  Scenario: Expose health endpoint
    When I get "/health" from management
    Then I receive a response with status 200

