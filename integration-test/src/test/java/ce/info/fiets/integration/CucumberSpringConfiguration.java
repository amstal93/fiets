package ce.info.fiets.integration;

import ce.info.fiets.integration.world.ResponseWorld;

import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.boot.test.context.SpringBootTest;

@CucumberContextConfiguration
@SpringBootTest(classes = {ResponseWorld.class})
public class CucumberSpringConfiguration {
}
