package ce.info.fiets;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = {"classpath:specification"},
    publish = true,
    plugin = {"pretty"})
public class CucumberIT {

}
